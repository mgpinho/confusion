'use strict';

module.exports = function(grunt) {

  // Use time-grunt to display the time it takes to execute a task
  require('time-grunt')(grunt);
  // Use jit-grunt to automatically load required grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  // Define the grunt configuration for the grunt object being passed into this function
  grunt.initConfig({

    // initConfig() method is configured similarly to a JSON object
    // sass task to update css files from an scss file
    sass: {
      dist: {
        files: {
          // 'output_file' : 'input_file'
          'styles/css/style.css': 'styles/scss/style.scss'
        }
      }
    },
    // Create a task that will copy content into 'dist'
    copy: {
      html: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: './',
            src: ['*.html'],
            dest: 'dist'
          }
        ]
      },
      fonts: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: './node_modules/font-awesome',
            src: ['fonts/*.*'],
            dest: 'dist'
          }
        ]
      }
    },
    //Create a tast that will clean out the 'dist' folder
    clean: {
      build: {
        src: ['dist/']
      }
    },
    //Configure the imagemin tasks
    imagemin: {
      dynamic: {
        files: [
          {
            expand: true,                             //Enables dynamic expansion
            cwd: './',                                //Src matches are relative to this path
            src: ['assets/images/*.{png, jpg, gif}'], //Actual patterns to match
            dest: 'dist/'                             //Destination path prefix
          }
        ]
      }
    },
    // Watch task to watch for any changes to scss files
    watch: {
      files: 'styles/scss/*.scss',
      tasks: ['sass']
    },
    // Browser sync to update the browser if any files are changed
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            '*.html',
            'styles/css/*.css',
            'scripts/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: "./"
          }
        }
      }
    },
    useminPrepare: {
      foo: {
        dest: 'dist',
        src: ['contactus.html', 'aboutus.html', 'index.html']
      },
      options: {
        flow: {
          steps: {
            css: ['cssmin'],
            js: ['uglify']
          },
          post: {
            css: [
              {
                name: 'cssmin',
                createConfig: function (context, block) {
                  var generated = context.options.generated;
                  generated.options = {
                    keepSpecialComments: 0,
                    rebase: false
                  };
                }
              }
            ]
          }
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {}
    },
    uglify: {
      dist: {}
    },
    cssmin: {
      dist: {}
    },
    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 20
      },
      release: {
        // filerev:release hashes(md5) all assets(images, js and css) in the dist directory
        files: [
          {
            src: [
              'dist/js/*.js',
              'dist/css/*.css'
            ]
          }
        ]
      }
    },
    // Usemin replaced all assets with their revved version in html and css files
    //options.assetDirs contains the directories for finding the assets according to their relative paths
    usemin: {
      html: [
        'dist/contactus.html',
        'dist/aboutus.html',
        'dist/index.html'
      ],
      options: {
        assetsDirs: [
          'dist',
          'dist/css',
          'dist/js'
        ]
      }
    },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true
        },
        files: {
          'dist/index.html': 'dist/index.html',
          'dist/contactus.html': 'dist/contactus.html',
          'dist/aboutus.html': 'dist/aboutus.html'
        }
      }
    }

  });

  // Tasks should be registered after the configuration
  grunt.registerTask('css', ['sass']);
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('build', ['clean', 'copy', 'imagemin', 'useminPrepare', 'concat', 'cssmin', 'uglify', 'filerev', 'usemin', 'htmlmin'])
};
