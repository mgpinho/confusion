$(document).ready(function() {
  // Change the interval time to 2s. The 2000 is in milliseconds so by changeing "interval:2000" we are saying "move the carousel every 2 seconds"
  $("#myCarousel").carousel({ interval: 2000 });

  // When the pause button is clicked, the carousel should be paused
  $("#carouselToggle").click(function() {
    //Check whether the carousel is currently playing or paused
    //If the "pause" button is currently displayed
    if ($("#carouselToggle").children("span").hasClass('fa-pause')) {

      // If the carousel is currently playing then pause
      $("#myCarousel").carousel('pause');

      // Next change the button from a pause button to a play button
      $("#carouselToggle").children("span").removeClass('fa-pause');
      $("#carouselToggle").children("span").addClass('fa-play');
    }
    // If there is a play button displayed
    // *Note* This if condition is unnecessary because there will realistically will only be 2 option (play and pause).
    //        This code is slightly unnecessary but it just reinforces the if/else condition
    else if ($("#carouselToggle").children("span").hasClass('fa-play')) {
      // If the "play" button is currently displayed then resume the "cycle" (make the carousel play again)
      $("#myCarousel").carousel('cycle');

      // Change the class from play to pause
      $("#carouselToggle").children("span").removeClass('fa-play');
      $("#carouselToggle").children("span").addClass('fa-pause');
    }
  });

  // Trigger the modals
  $("#loginButton").click(function() {
    $("#loginModal").modal('show');
  });
  $("#reserveTableButton").click(function() {
    $("#reservationModal").modal('show');
  });
});
